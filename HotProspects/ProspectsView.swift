//
//  ProspectsView.swift
//  HotProspects
//
//  Created by Hariharan S on 29/06/24.
//

import SwiftUI
import SwiftData
import CodeScanner
import UserNotifications

enum FilterType {
    case none, contacted, uncontacted
}

struct ProspectsView: View {
    let filter: FilterType
    
    let testPersons = [
        "Test Person 1\ntextperson_1@gmail.com",
        "Test Person 2\ntextperson_2@gmail.com",
        "Test Person 3\ntextperson_3@gmail.com",
        "Test Person 4\ntextperson_4@gmail.com",
        "Test Person 5\ntextperson_5@gmail.com"
    ]
    
    @State private var isShowingScanner = false
    @State private var selectedProspects = Set<Prospect>()
    
    @Environment(\.modelContext) var modelContext
    @Query(sort: \Prospect.name) var prospects: [Prospect]
    
    init(filter: FilterType) {
        self.filter = filter

        if filter != .none {
            let showContactedOnly = filter == .contacted

            _prospects = Query(
                filter: #Predicate {
                    $0.isContacted == showContactedOnly
                },
                sort: [SortDescriptor(\Prospect.name)]
            )
        }
    }
    
    var title: String {
        switch filter {
        case .none:
            "Everyone"
        case .contacted:
            "Contacted people"
        case .uncontacted:
            "Uncontacted people"
        }
    }
    
    var body: some View {
        NavigationStack {
            List(prospects, selection: $selectedProspects) { prospect in
                VStack(alignment: .leading) {
                    Text(prospect.name)
                        .font(.headline)
                    Text(prospect.emailAddress)
                        .foregroundStyle(.secondary)
                }
                .swipeActions {
                    if prospect.isContacted {
                        Button(
                            "Mark Uncontacted",
                            systemImage: "person.crop.circle.badge.xmark"
                        ) {
                            prospect.isContacted.toggle()
                        }
                        .tint(.blue)
                    } else {
                        Button(
                            "Mark Contacted",
                            systemImage: "person.crop.circle.fill.badge.checkmark"
                        ) {
                            prospect.isContacted.toggle()
                        }
                        .tint(.green)
                    }
                    Button(
                        "Delete",
                        systemImage: "trash",
                        role: .destructive
                    ) {
                        self.modelContext.delete(prospect)
                    }
                    Button(
                        "Remind Me",
                        systemImage: "bell"
                    ) {
                        self.addNotification(for: prospect)
                    }
                    .tint(.orange)
                }
                .tag(prospect)
            }
                .navigationTitle(title)
                .toolbar {
                    ToolbarItem(placement: .topBarLeading) {
                        EditButton()
                    }
                    if selectedProspects.isEmpty == false {
                        ToolbarItem(placement: .bottomBar) {
                            Button("Delete Selected", action: delete)
                        }
                    }
                    ToolbarItem(placement: .topBarTrailing) {
                        Button("Scan", systemImage: "qrcode.viewfinder") {
                            isShowingScanner = true
                        }
                    }
                }
                .sheet(isPresented: $isShowingScanner) {
                    CodeScannerView(
                        codeTypes: [.qr],
                        simulatedData: testPersons.randomElement() ?? "",
                        completion: handleScan
                    )
                }
        }
    }
    
    func handleScan(result: Result<ScanResult, ScanError>) {
        self.isShowingScanner = false
        switch result {
        case let .success(result):
            let details = result.string.components(separatedBy: "\n")
            guard details.count == 2
            else {
                return
            }

            let person = Prospect(
                name: details[0],
                emailAddress: details[1],
                isContacted: false
            )
            self.modelContext.insert(person)
        case let .failure(error):
            fatalError("Scanning failed: \(error.localizedDescription)")
        }
    }
    
    func delete() {
        for prospect in selectedProspects {
            modelContext.delete(prospect)
        }
    }
    
    func addNotification(for prospect: Prospect) {
        let center = UNUserNotificationCenter.current()

        let addRequest = {
            let content = UNMutableNotificationContent()
            content.title = "Contact \(prospect.name)"
            content.subtitle = prospect.emailAddress
            content.sound = UNNotificationSound.default

            var dateComponents = DateComponents()
            dateComponents.hour = 9
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)

            let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
            center.add(request)
        }

        center.getNotificationSettings { settings in
            if settings.authorizationStatus == .authorized {
                addRequest()
            } else {
                center.requestAuthorization(options: [.alert, .badge, .sound]) { success, error in
                    if success {
                        addRequest()
                    } else if let error {
                        print(error.localizedDescription)
                    }
                }
            }
        }
    }
}

#Preview {
    ProspectsView(filter: .none)
}
