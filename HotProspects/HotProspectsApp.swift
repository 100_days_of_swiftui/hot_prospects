//
//  HotProspectsApp.swift
//  HotProspects
//
//  Created by Hariharan S on 29/06/24.
//

import SwiftData
import SwiftUI

@main
struct HotProspectsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
        .modelContainer(for: Prospect.self)
    }
}
